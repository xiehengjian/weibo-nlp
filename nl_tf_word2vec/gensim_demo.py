# -*- coding:utf-8 -*-
import codecs
from gensim.models import word2vec

# 读取数据
class Sentences(object):
    def __init__(self, file_path):
        self.file_path = file_path
    def __iter__(self):
        # 读取all中的每行数据
        for line in codecs.open(self.file_path, 'r','utf8'):
            # 迭代返回每行数据中的每个分词
            yield line.split()

if __name__=="__main__":

    # 定义输入输出
    inpath = 'data/all'
    outpath1 = 'word2vec.bin'
    outpath2 = 'word2vec.vector'

    # 读取all文件中的数据
    sentences = Sentences(inpath)
    # 直接调用gensim中的word2vec模型
    model = word2vec.Word2Vec(sentences)

    # 模型保存
    model.save(outpath1)
    model.wv.save_word2vec_format(outpath2,binary=False)

    # 验证一下“我”的word2vec词向量
    print(model['我'])