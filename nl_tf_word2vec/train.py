# -*- coding:utf-8 -*-
import tensorflow as tf
import data_util
from model import Word2Vec


FLAGS = tf.app.flags.FLAGS
tf.app.flags.DEFINE_integer("embedding_size",100,"size of word embedding")
tf.app.flags.DEFINE_integer("skip_window",2,"How many words to consider left and right.")
tf.app.flags.DEFINE_integer("num_skips",2," How many times to reuse an input to generate a label.")
tf.app.flags.DEFINE_integer("num_sample",2,"Number of negative examples to sample.")
tf.app.flags.DEFINE_integer("batch_size",12,"batch size")
tf.app.flags.DEFINE_float("learning_rate",0.01,"learning rate")
tf.app.flags.DEFINE_integer("decay_steps",6000,"how many steps before decay learning rate.")
tf.app.flags.DEFINE_float("decay_rate", 0.65, "Rate of decay for learning rate.")
tf.app.flags.DEFINE_string("mode","train","choose the mode")
tf.app.flags.DEFINE_string("file_dir","data/","the path of the train data")
tf.app.flags.DEFINE_integer("min_frequency", 3, "The minimum frequency of words")

tf.app.flags.DEFINE_integer("num_epochs", 2, "number of epochs to run.")
tf.app.flags.DEFINE_string("ckpt_dir","checkpoint/","checkpoint location for the model")


def get_similarity(sess,w2v, wids, top_k=10):
    feed_dict = {w2v.inputs:wids}
    sim_matrix = sess.run(w2v.similarity_val,feed_dict)
    nearst_ids = (sim_matrix[0,:]).argsort()[1:top_k + 1]
    return nearst_ids

def main(_):
    # 获取数据预处理结果、数据集大小
    train, word2idx, idx2word = data_util.load_dataset(FLAGS.file_dir, FLAGS.min_frequency)
    vocab_size = len(word2idx)
    print("vocab_size:",vocab_size)
    # 设定gpu训练
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True


    with tf.Session(config=config) as sess:
        # 搭建word2vec结构
        w2v = Word2Vec(FLAGS.embedding_size,FLAGS.skip_window, FLAGS.num_skips, FLAGS.num_sample,
                       FLAGS.batch_size, vocab_size, FLAGS.learning_rate, FLAGS.decay_steps, FLAGS.decay_rate)
        # 初始化tf训练器
        init = tf.global_variables_initializer()
        saver = tf.train.Saver(max_to_keep=1)
        print('Initializing Variables')
        sess.run(init)
        # 执行epoch轮次训练
        curr_epoch = sess.run(w2v.epoch_step)
        for epoch in range(curr_epoch, FLAGS.num_epochs):
            one_epoch_steps = len(train.datalist) / FLAGS.batch_size
            step = 1
            while step <= one_epoch_steps:
                # 获取一个批次的数据
                batch_data, label_data = train.next_batch(FLAGS.batch_size, FLAGS.num_skips, FLAGS.skip_window)
                # 数据封装
                feed_dict = {w2v.inputs:batch_data, w2v.labels:label_data}
                # 将此batch数据送入word2vec模型训练，得到loss值
                curr_loss, _ = sess.run([w2v.loss_val,w2v.train_op],feed_dict)
                print(curr_loss)
                step += 1
            # # 保存模型
            save_path = FLAGS.ckpt_dir + "model.ckpt"
            saver.save(sess, save_path, global_step=epoch)

        r_ids = get_similarity(sess,w2v,[1]+[0]*11,2)
        r_words = [idx2word[index] for index in r_ids]
        print(r_words)

if __name__=="__main__":
    tf.app.run()
