# -*- coding:utf-8 -*-
import tensorflow as tf
import math

class Word2Vec:

    def __init__(self, embedding_size,skip_window,num_skips, num_sample, batch_size,vocab_size, learning_rate,
                 decay_steps, decay_rate, mode="train"):
        self.inputs = tf.placeholder(tf.int32, shape=[batch_size])
        self.labels = tf.placeholder(tf.int32,shape=[batch_size,1])
        self.embedding_size = embedding_size # 词向量大小
        self.skip_window = skip_window       # 窗口大小
        self.num_skips = num_skips           # 表示在两侧窗口内总共取多少个词，数量小于2*skip_window
        self.num_sample = num_sample         # 负样本数目
        self.learning_rate = learning_rate   # 学习率
        self.global_step = tf.Variable(0, trainable=False, name="Global_Step")
        self.epoch_step = tf.Variable(0, trainable=False, name="Epoch_Step")
        self.epoch_increment = tf.assign(self.epoch_step, tf.add(self.epoch_step, tf.constant(1))) # epoch的增加量
        self.decay_steps = decay_steps       # 学习步长
        self.decay_rate = decay_rate         # 学习率调整率
        self.vocab_size = vocab_size         # 词汇总量
        self.mode = mode                     # 训练或者测试模式

        self.instantiate_weights()
        self.embed_words, self.logits = self.inference()
        self.labels_one_hot = tf.one_hot(self.labels, self.vocab_size)  # one-hot形式的标签
        self.loss_val = self.loss()
        self.train_op = self.train()
        self.similarity_val = self.similarity()

    def instantiate_weights(self):
        # 定义一个vocab_size*embedding_size的随机输入矩阵，取值范围为-1到1
        self.EmbeddingMatrix = tf.Variable(tf.random_uniform([self.vocab_size, self.embedding_size], -1.0, 1.0))
        # 按正态分布随机构建一个vocab_size*embedding_size的nce loss权重值矩阵
        self.nce_weights = tf.Variable(tf.truncated_normal([self.vocab_size, self.embedding_size],
                                                           stddev=1.0/math.sqrt(self.embedding_size)))
        # 初始化一个全0的长度为vocab_size的biases偏置数组
        self.nce_biases = tf.Variable(tf.zeros([self.vocab_size]))

    def inference(self):
        # 输入一个batch的数据
        embed_words = tf.nn.embedding_lookup(self.EmbeddingMatrix, self.inputs)
        # 输入*权值 + 偏置
        logits = tf.nn.bias_add(tf.matmul(embed_words, tf.transpose(self.nce_weights)),self.nce_biases)
        return embed_words, logits

    def loss(self):
        with tf.name_scope("loss"):
            # 训练模式下的loss设定
            if self.mode == "train":
                # 直接调用tf库中的nce loss接口
                loss = tf.reduce_mean(tf.nn.nce_loss(
                    weights=self.nce_weights,
                    biases=self.nce_biases,
                    inputs=self.embed_words,
                    labels=self.labels,
                    num_sampled=self.num_sample,
                    num_classes=self.vocab_size))
            # 非训练模式下，采用二值交叉熵loss
            else:
                # 讲label标签转为one-hot格式再计算交叉熵损失
                losses = tf.nn.sigmoid_cross_entropy_with_logits(labels=self.labels_one_hot, logits=self.logits)
                loss = tf.reduce_mean(losses, axis=1)
        return loss

    # 执行训练的配置
    def train(self):
        # 学习率的封装配置
        learning_rate = tf.train.exponential_decay(self.learning_rate, self.global_step, self.decay_steps,
                                                   self.decay_rate, staircase=True)
        # 训练优化算法配置
        train_op = tf.contrib.layers.optimize_loss(self.loss_val, global_step=self.global_step,
                                                   learning_rate=learning_rate, optimizer="Adam")
        return train_op


    # 利用余弦距离计算batch个和全部输入向量的相似度
    def similarity(self):
        norm = tf.sqrt(tf.reduce_sum(tf.square(self.EmbeddingMatrix), 1, keepdims=True))
        normalized_embeddings = self.EmbeddingMatrix / norm
        valid_embeddings = tf.nn.embedding_lookup(normalized_embeddings,
                                                  self.inputs)
        similarity = tf.matmul(
            valid_embeddings, normalized_embeddings, transpose_b=True)
        return similarity