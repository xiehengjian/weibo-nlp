# -*- coding:utf-8 -*-
import numpy as np
import collections
import random
import os
import codecs

## 封装数据集，构建训练数据迭代器
class Dataset:
    def __init__(self, datalist, currentIdx):
        self.datalist = datalist
        self.currentIdx = currentIdx

    # 编写迭代器迭代方法，返回截取的一个批次的数据
    def next_batch(self, batch_size, num_skips, skip_window):
        # 确保batch size和num_skips是整数倍的关系
        assert batch_size % num_skips == 0
        # num_skips表示在两侧窗口内总共取多少个词，数量小于2*skip_window
        assert num_skips <= 2*skip_window

        batch_data = np.ndarray(shape=(batch_size), dtype=np.int32)
        label_data = np.ndarray(shape=(batch_size,1), dtype=np.int32)
        # [skip_window target  skip_window]
        span = 2 * skip_window +1
        # 不断地保存span个单词到buffer中，然后不断往后滑动。其中buffer[skip_window]就是中心词
        buffer = collections.deque(maxlen=span)

        # 设置数据起始位置
        if self.currentIdx + span > len(self.datalist):
            self.currentIdx = 0
        # 按batch size大小添加数据
        buffer.extend(self.datalist[self.currentIdx: self.currentIdx+span])
        # 更新数据起始位置
        self.currentIdx += span

        for i in range(batch_size // num_skips):
            context_words = [w for w in range(span) if w != skip_window]
            word_to_use = random.sample(context_words, num_skips)
            # 读取中心词，并保存对应的label
            for j, context_word in enumerate(word_to_use):
                batch_data[i * num_skips + j] = buffer[skip_window]
                label_data[i * num_skips + j, 0] = buffer[context_word]
            # buffer滑动一格，重置数据起始位置到下一个batch
            if self.currentIdx == len(self.datalist):
                buffer.extend(self.datalist[0:span])
                self.currentIdx = span
            else:
                buffer.append(self.datalist[self.currentIdx])
                self.currentIdx += 1
        self.currentIdx = (self.currentIdx + len(self.datalist) - span) % len(self.datalist)
        return batch_data, label_data


def load_dataset(file_dir,min_frequensy):
    words = []
    vocab = [['unk', 0]]
    for fname in os.listdir(file_dir):
        for line in codecs.open(os.path.join(file_dir, fname),'r','utf8'):
            # 获取all文件中的所有分词处理后的句子，并汇总成一个包含所有分词句子的列表corpus
            _split = line.strip().split()
            words.extend(_split)
    # 只保留词频高于设定值的分词
    vocab.extend([list(item) for item in collections.Counter(words).most_common() if item[1] >= min_frequensy])
    # 每个分词附上id编号
    word2idx = {}
    idx2word = {}
    for word, _ in vocab:
        ids = len(word2idx)
        word2idx[word] = len(word2idx)
        idx2word[ids] = word

    # 构建正样本词典和负样本词典
    datalist = []
    unk_count = 0
    for word in words:
        # 如果该分词在分词编号字典中出现过，则记录该分词的id编号
        if word in word2idx:
            idx = word2idx[word]
        # 如果未出现，则记录未保存分词数量+1
        else:
            idx = 0
            unk_count = unk_count + 1
        # 将保留下来的分词对应的id编号写入datalist列表中
        datalist.append(idx)
    vocab[0][1] = unk_count
    # 封装数据集，构建训练数据迭代器
    train = Dataset(datalist, 0)
    return train, word2idx, idx2word
