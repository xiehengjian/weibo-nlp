# -*- coding:utf-8 -*-

import tensorflow as tf
from tensorflow.contrib import rnn


class TextRNN:
    def __init__(self,num_classes, learning_rate, batch_size, decay_steps, decay_rate,sequence_length,
                 vocab_size,embed_size,hidden_size,initializer=tf.random_normal_initializer(stddev=0.1)):
        self.num_classes = num_classes          # 情绪标签的类别总数
        self.learning_rate = learning_rate      # 学习率
        self.batch_size = batch_size            # batch大小
        self.decay_steps = decay_steps          # 学习率调整步长
        self.decay_rate = decay_rate            # 学习率调整率
        self.sequence_length = sequence_length  # 句子长度
        self.vocab_size = vocab_size            # 词汇总量
        self.embed_size = embed_size            # 词向量长度
        self.hidden_size = hidden_size          # 模型的隐藏层神经元数
        self.initializer = initializer          # 初始化方法

        # add placeholder (X,label)
        self.input_x = tf.placeholder(tf.int32, [None, self.sequence_length], name="input_x")
        self.input_y = tf.placeholder(tf.int32, [None], name="input_y")
        self.mask_x = tf.placeholder(tf.float32, [None, self.sequence_length])
        self.dropout_keep_prob = tf.placeholder(tf.float32, name="dropout_keep_prob")

        self.global_step = tf.Variable(0, trainable=False, name="Global_Step")
        self.epoch_step = tf.Variable(0, trainable=False, name="Epoch_Step")
        self.epoch_increment = tf.assign(self.epoch_step, tf.add(self.epoch_step, tf.constant(1)))
        self.decay_steps, self.decay_rate = decay_steps, decay_rate

        self.instantiate_weights()
        self.logits = self.inference()  # [None, self.label_size]. main computation graph is here.
        self.loss_val = self.loss()
        self.train_op = self.train()
        self.logits_sofmax = tf.nn.softmax(self.logits)
        self.predictions = tf.argmax(self.logits_sofmax, 1, name="predictions")  # shape:[None,]
        correct_prediction = tf.equal(tf.cast(self.predictions, tf.int32), self.input_y)
        self.loss_val = self.loss()
        self.accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32), name="Accuracy")

    def instantiate_weights(self):
        # 初始化textrnn的embedding层
        self.EmbeddingMatrix = tf.get_variable("EmbeddingMatrix",shape=[self.vocab_size,self.embed_size],initializer=self.initializer)
        # 初始化一个num_filters_total*num_classes的权重值矩阵w
        self.W_projection = tf.get_variable("W_projection", shape=[self.hidden_size*2, self.num_classes],
                                            initializer=self.initializer)  # [embed_size,label_size]
        # 初始化一个num_classes的偏置矩阵b
        self.b_projection = tf.get_variable("b_projection", shape=[self.num_classes])  # [label_size]

    def inference(self):
        # 输入句子矩阵，每行是词向量
        self.embed_words = tf.nn.embedding_lookup(self.EmbeddingMatrix,self.input_x)
        # 搭建bi-lstm层
        lstm_fw_cell = rnn.BasicLSTMCell(num_units=self.hidden_size)
        lstm_bw_cell = rnn.BasicLSTMCell(num_units=self.hidden_size)
        if self.dropout_keep_prob is not None:
            lstm_fw_cell = rnn.DropoutWrapper(lstm_fw_cell,output_keep_prob=self.dropout_keep_prob)
            lstm_bw_cell = rnn.DropoutWrapper(lstm_bw_cell,output_keep_prob=self.dropout_keep_prob)
        outputs, _ = tf.nn.bidirectional_dynamic_rnn(lstm_fw_cell, lstm_bw_cell, self.embed_words, dtype=tf.float32)
        # 将bi-listm的输出进行concat
        output_rnn = tf.concat(outputs, axis=2)#[batch_size,sequence_length,hidden_size*2]
        # 取均值
        self.output_rnn_mean = tf.reduce_sum(output_rnn*self.mask_x[:, :, None], axis=1)  # [batch_size,hidden_size*2]
        print("output_rnn_last:", self.output_rnn_mean)
        # w*h+b
        logits = tf.matmul(self.output_rnn_mean, self.W_projection) + self.b_projection
        return logits

    # loss函数设定
    def loss(self, l2_lambda=0.0001):
        with tf.name_scope("loss"):
            # 设定交叉熵损失函数
            losses = tf.nn.sparse_softmax_cross_entropy_with_logits(labels=self.input_y,
                                                                    logits=self.logits)
            loss = tf.reduce_mean(losses)
            # 添加l2正则项
            l2_losses = tf.add_n(
                [tf.nn.l2_loss(v) for v in tf.trainable_variables() if 'bias' not in v.name]) * l2_lambda
            # 总loss函数为交叉熵损失和l2正则项个和
            loss = loss + l2_losses
        return loss

    # 执行训练的配置
    def train(self):
        # 学习率的封装配置
        learning_rate = tf.train.exponential_decay(self.learning_rate, self.global_step, self.decay_steps,self.decay_rate, staircase=True)
        # 训练优化算法配置
        train_op = tf.contrib.layers.optimize_loss(self.loss_val, global_step=self.global_step,learning_rate=learning_rate, optimizer="Adam")
        return train_op