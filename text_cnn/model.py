# -*- coding:utf-8 -*-
import tensorflow as tf

class TextCNN:

    def __init__(self, filter_sizes, num_filters, num_classes, learning_rate, batch_size, decay_steps, decay_rate, sequence_length, vocab_size,embed_size,
                 initializer=tf.random_normal_initializer(stddev=0.1)):
        self.num_classes = num_classes   # 情绪标签的类别总数
        self.batch_size = batch_size
        self.sequence_length = sequence_length  # 句子长度
        self.vocab_size = vocab_size            # 词汇总量
        self.embed_size = embed_size            # 词向量长度
        self.learning_rate = tf.Variable(learning_rate, trainable=False, name="learning_rate")  # 学习率
        self.filter_sizes = filter_sizes  # 卷积核大小的列表，如：[3,4,5]
        self.num_filters = num_filters    # 每层的卷积核个数
        self.initializer = initializer    # 初始化方法
        self.num_filters_total = self.num_filters * len(filter_sizes)  # 网络全部卷积核个数

        # add placeholder (X,label)Epoch_Step
        self.input_x = tf.placeholder(tf.int32, [None, self.sequence_length], name="input_x")  # X
        self.input_y = tf.placeholder(tf.int32, [None, ], name="input_y")  # y:[None,num_classes]
        self.dropout_keep_prob = tf.placeholder(tf.float32, name="dropout_keep_prob")

        self.global_step = tf.Variable(0, trainable=False, name="Global_Step")
        self.epoch_step = tf.Variable(0, trainable=False, name="Epoch_Step")
        self.epoch_increment = tf.assign(self.epoch_step, tf.add(self.epoch_step, tf.constant(1)))
        self.decay_steps, self.decay_rate = decay_steps, decay_rate

        self.instantiate_weights()
        self.logits = self.inference()  # [None, self.label_size]. main computation graph is here.
        self.loss_val = self.loss()
        self.train_op = self.train()
        self.logits_softmax = tf.nn.softmax(self.logits)
        self.predictions = tf.argmax(self.logits_softmax, 1, name="predictions")  # shape:[None,]
        correct_prediction = tf.equal(tf.cast(self.predictions, tf.int32), self.input_y)
        self.loss_val = self.loss()
        self.accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32), name="Accuracy")


    def instantiate_weights(self):
        # 初始化textcnn的embedding层
        self.Embedding = tf.get_variable("Embedding", shape=[self.vocab_size, self.embed_size],
                                         initializer=self.initializer)  # [vocab_size,embed_size] tf.random_uniform([self.vocab_size, self.embed_size],-1.0,1.0)
        # 初始化一个num_filters_total*num_classes的权重值矩阵w
        self.W_projection = tf.get_variable("W_projection", shape=[self.num_filters_total, self.num_classes],
                                            initializer=self.initializer)  # [embed_size,label_size]
        # 初始化一个num_classes的偏置矩阵b
        self.b_projection = tf.get_variable("b_projection", shape=[self.num_classes])  # [label_size]

    def inference(self):
        #输入句子矩阵，每行是词向量
        self.embedded_words = tf.nn.embedding_lookup(self.Embedding, self.input_x)
        self.sentence_embeddings_expanded = tf.expand_dims(self.embedded_words,
                                                           -1)
        pooled_outputs = []
        # textrnn网络搭建
        # 经过kernel_sizes = (1，2, 3, 4，5)的一维卷积层
        for i, filter_size in enumerate(self.filter_sizes):
            with tf.name_scope("convolution-pooling-%s" % filter_size):
                filter = tf.get_variable("filter-%s" % filter_size, [filter_size, self.embed_size, 1, self.num_filters],
                                         initializer=self.initializer)
                # 卷积层
                conv = tf.nn.conv2d(self.sentence_embeddings_expanded, filter, strides=[1, 1, 1, 1], padding="VALID",
                                    name="conv")
                b = tf.get_variable("b-%s" % filter_size, [self.num_filters])
                h = tf.nn.relu(tf.nn.bias_add(conv, b), "relu")
                #maxpooling层，这样不同长度句子经过pooling层之后都能变成定长的表示。
                pooled = tf.nn.max_pool(h, ksize=[1, self.sequence_length - filter_size + 1, 1, 1],
                                        strides=[1, 1, 1, 1], padding='VALID',
                                        name="pool")
                pooled_outputs.append(pooled)
        self.h_pool = tf.concat(pooled_outputs,3)
        self.h_pool_flat = tf.reshape(self.h_pool, [-1, self.num_filters_total])
        # dropout层
        with tf.name_scope("dropout"):
            self.h_drop = tf.nn.dropout(self.h_pool_flat, keep_prob=self.dropout_keep_prob)
        # H*W+b
        with tf.name_scope("output"):
            logits = tf.matmul(self.h_drop, self.W_projection) + self.b_projection
        return logits

    # loss函数设定
    def loss(self, l2_lambda=0.0001):
        with tf.name_scope("loss"):
            # 设定交叉熵损失函数
            losses = tf.nn.sparse_softmax_cross_entropy_with_logits(labels=self.input_y,
                                                                    logits=self.logits);
            loss = tf.reduce_mean(losses)
            # 添加l2正则项
            l2_losses = tf.add_n(
                [tf.nn.l2_loss(v) for v in tf.trainable_variables() if 'bias' not in v.name]) * l2_lambda
            # 总loss函数为交叉熵损失和l2正则项个和
            loss = loss + l2_losses
        return loss

    # 执行训练的配置
    def train(self):
        # 学习率的封装配置
        learning_rate = tf.train.exponential_decay(self.learning_rate, self.global_step, self.decay_steps,self.decay_rate, staircase=True)
        # 训练优化算法配置
        train_op = tf.contrib.layers.optimize_loss(self.loss_val, global_step=self.global_step,learning_rate=learning_rate, optimizer="Adam")
        return train_op