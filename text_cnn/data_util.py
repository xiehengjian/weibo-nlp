# -*- coding:utf-8 -*-
import codecs
import random
import gensim
import numpy as np

DEFAULT_WORD2VEC_PATH = "data/word2vec.bin"


## 读取数据
def load_vocab_path(vocabulary_path):
    vocabs = []
    #读取数据中的每一行
    for line in codecs.open(vocabulary_path,'r','utf-8'):
        # 将本行数据写入到列表中
        vocabs.append(line.strip())
    return vocabs

## 构建分词转id、id转分词两个字典
def create_vocabulary(vocabulary_path):
    vocabulary_word2index = {}
    vocabulary_index2word = {}
    # 设置一个填充字符，命名为“PAD_ID”，对应id编号为0
    # 在分词转id字典中：{‘PAD_ID’:0}
    vocabulary_word2index['PAD_ID'] = 0
    # 在id转分词字典中：{0:‘PAD_ID’}
    vocabulary_index2word[0] = 'PAD_ID'

    # 读取vocab中的每个数据（每个分词）
    for i, vocab in enumerate(load_vocab_path(vocabulary_path)):
        # 给每个分词添加id编号，生成相应字典，id编号从1开始计数
        # 如，分词转id字典：{分词1:1，分词2:2,...}
        vocabulary_word2index[vocab] = i + 1
        # 如，id转分词字典：{1：分词1,2：分词2,...}
        vocabulary_index2word[i + 1 ] = vocab
    return vocabulary_word2index, vocabulary_index2word

## 构建标签转id、id转标签两个字典
def create_voabulary_label(lable_path):
    vocabulary_word2index_label = {}
    vocabulary_index2word_label = {}
    # 读取label标签文件，遍历所有标签
    for i,label in enumerate(load_vocab_path(lable_path)):
        # 给每个标签添加id编号，生成相应字典，id编号从1开始计数
        # 如，标签转id字典：{标签1:1，标签2:2,...}
        vocabulary_word2index_label[label]=i
        # 如，id转标签字典：{1：标签1,2：标签2,...}
        vocabulary_index2word_label[i]=label
    return vocabulary_word2index_label, vocabulary_index2word_label

## 构建分词转id、id转分词两个字典，以及所有分词的词向量列表
def create_vocabulary_pred_embedding(vocabulary_path,vec_size,word2vec_model_path=None):

    vocabulary_word2index = {}
    vocabulary_index2word = {}
    word_embeddings = []
    # 确认word2vec模型路径
    if word2vec_model_path is None:
        word2vec_model_path = DEFAULT_WORD2VEC_PATH
    print("create vocabulary. word2vec_model_path:",word2vec_model_path)
    # 利用gensiom加载训练好的word2vec词向量模型
    model = gensim.models.Word2Vec.load(word2vec_model_path)

    # 设置一个填充字符，命名为“PAD_ID”，对应id编号为0
    # 在分词转id字典中：{‘PAD_ID’:0}
    vocabulary_word2index['PAD_ID'] = 0
    # 在id转分词字典中：{0:‘PAD_ID’}
    vocabulary_index2word[0] = 'PAD_ID'
    # 初始化（置零）词向量列表，长度为vec_size
    word_embeddings.append([0]*vec_size)

    # 读取vocab中的每个数据（每个分词）
    for i, vocab in enumerate(load_vocab_path(vocabulary_path)):
        # 给每个分词添加id编号，生成相应字典，id编号从1开始计数
        # 如，分词转id字典：{分词1:1，分词2:2,...}
        vocabulary_word2index[vocab] = i + 1
        # 如，id转分词字典：{1：分词1,2：分词2,...}
        vocabulary_index2word[i + 1 ] = vocab
        # 如果当前分词在word2vec词向量模型中存在，那么将该词的词向量写入到word_embeddings列表中
        if vocab in model:
            word_embeddings.append(model[vocab])
        # 如果当前分词在word2vec词向量模型中不存在，那么将长度为vec_size的全0数组写入到word_embeddings列表中
        else:
            word_embeddings.append([0]*vec_size)
    return vocabulary_word2index, vocabulary_index2word, np.array(word_embeddings)

## 构建封装每个分词的类
class Word:
    def __init__(self, word, wordindex):
        self.word = word
        self.wordindex = wordindex

## 构建封装一段文本的类
class Text:
    def __init__(self,wordlist,label):
        self.wordlist = wordlist
        self.label = label

## 获取一段指定长度的文本及对应的情绪标签
def readText(fpath,vocabulary_word2index,vocabulary_word2index_label,maxlen):
    textlist=[]
    # 读取训练数据
    for line in codecs.open(fpath, 'r', 'utf-8'):
        # 获取训练数据中的句子和对应情绪标签
        item = line.strip().split('|')
        if len(item)==2:
            # 将句子按照分词列表的形式保存
            words = item[0].strip().split(' ')
            wordlist = []
            # 遍历该句子中的每个分词
            for w in words:
                # 如果当前分词在分词转id字典中存在，则将该分词及对应的id标号按照Word类封装，并写入wordlist列表
                if w in vocabulary_word2index:
                    wordlist.append(Word(w, vocabulary_word2index[w]))
                # 如果当不存在，则将该分词的id编号设为0，按照Word类封装写入wordlist列表
                else:
                    wordlist.append(Word(w, vocabulary_word2index['PAD_ID']))
            # 如果wordlist长度小于maxlen，则用填充词PAD_ID及对应id编号0，填充到wordlist长度等于maxlen为止
            while len(wordlist) < maxlen:
                wordlist.append(Word('PAD_ID',vocabulary_word2index['PAD_ID']))
            # 将长度为maxlen的wordlist列表及该句子对应的情绪标签id编号，按照Text类封装，并写入textlist列表
            textlist.append(Text(wordlist[:maxlen],vocabulary_word2index_label[item[1]]))
        else:
            print("this sample is error："+line)
    return textlist

## 封装数据集，构建训练数据迭代器
class Dataset:
    def __init__(self, textlist, currentIdx):
        self.textlist = textlist
        self.currentIdx = currentIdx

    # 编写迭代器迭代方法，返回截取的一个批次的数据
    def nextBatch(self, batch_size):
        # 如果文本列表的长度小于batch_size的设定，那么直接打乱数据
        if self.currentIdx + batch_size > len(self.textlist):
            random.shuffle(self.textlist)
            self.currentIdx = 0
        startId = self.currentIdx
        labels = []
        idxs1 = []
        # 按照batch_size的大小分别添加数据的标签id编号、分词id编号到labels和idxs1列表中
        for _ in range(batch_size):
            labels.append(self.textlist[startId + _].label)
            idss1 = []
            for item in self.textlist[startId + _].wordlist:
                idss1.append(item.wordindex)
            idxs1.append(idss1)
        self.currentIdx += batch_size
        return idxs1, labels

## 加载数据集的接口
def load_dataset(vocabulary_word2index,vocabulary_word2index_label,maxlen,valid_portion=0.25,data_path='data/train.txt'):
    # 获取一段文本数据
    textlist = readText(data_path,vocabulary_word2index,vocabulary_word2index_label,maxlen)
    # 计算文本的长度
    samplelen = len(textlist)
    # 打乱数据
    random.shuffle(textlist)
    # 封装数据集，构建训练数据和验证数据迭代器
    train = Dataset(textlist[:int((1-valid_portion)*samplelen)], 0)
    valid = Dataset(textlist[int((1-valid_portion)*samplelen)+1:], 0)
    return train, valid