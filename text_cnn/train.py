# -*- coding:utf-8 -*-
import tensorflow as tf
import data_util
from model import TextCNN
import os
import numpy as np
import sklearn.metrics as metrics

#configuration
FLAGS=tf.app.flags.FLAGS
tf.app.flags.DEFINE_integer("num_classes",8,"number of label")
tf.app.flags.DEFINE_float("learning_rate",0.001,"learning rate")
tf.app.flags.DEFINE_integer("batch_size", 128, "Batch size for training/evaluating.")
tf.app.flags.DEFINE_integer("decay_steps", 6000, "how many steps before decay learning rate.")
tf.app.flags.DEFINE_float("decay_rate", 0.65, "Rate of decay for learning rate.")
tf.app.flags.DEFINE_string("ckpt_dir","checkpoint/","checkpoint location for the model")
tf.app.flags.DEFINE_integer("sentence_len",27,"max sentence length")
tf.app.flags.DEFINE_integer("embed_size",100,"embedding size")
tf.app.flags.DEFINE_integer("num_epochs",12,"number of epochs to run.")
tf.app.flags.DEFINE_integer("validate_every", 1, "Validate every validate_every epochs.")
tf.app.flags.DEFINE_boolean("use_embedding",True,"whether to use embedding or not.")
tf.app.flags.DEFINE_integer("num_filters", 128, "number of filters")
tf.app.flags.DEFINE_float("dropout_keep_prob",0.5,"TextCNN.dropout_keep_prob")

tf.app.flags.DEFINE_string("data_path","data/train","path of traning data.")
tf.app.flags.DEFINE_string("test_path","data/test","path of traning data.")
tf.app.flags.DEFINE_string("vocab_path","data/vocab","path of vocabulary.")
tf.app.flags.DEFINE_string("label_path","data/label","path of labeldit.")
tf.app.flags.DEFINE_string("word2vec_model_path","data/word2vec.bin","word2vec's vocabulary and vectors")
tf.app.flags.DEFINE_string("filter_sizes","1,2,3,4,5","")
tf.app.flags.DEFINE_float("valid_portion",0.1,"valid portion")


def main(_):
    # 获取数据预处理结果，得到分词转id编号、id编号转分词两个字典
    vocabulary_word2index, vocabulary_index2word, word_embeddings = None, None, None
    # 如果不需要得到每个词的word2vec词向量，调用create_vocabulary函数
    if not FLAGS.use_embedding:
        vocabulary_word2index, vocabulary_index2word = data_util.create_vocabulary(FLAGS.vocab_path)
    # 如果需要得到每个词的word2vec词向量，调用create_vocabulary_pred_embedding函数
    else:
        vocabulary_word2index, vocabulary_index2word, word_embeddings = data_util.create_vocabulary_pred_embedding(FLAGS.vocab_path, FLAGS.embed_size, FLAGS.word2vec_model_path)
    # 获取词汇量大小
    vocab_size = len(vocabulary_word2index)
    print("cnn_model.vocab_size:", vocab_size)
    # 获取标签预处理结果，得到标签转id编号、id编号转标签两个字典
    vocabulary_word2index_label, vocabulary_index2word_label = data_util.create_voabulary_label(FLAGS.label_path)

    # 生成训练、验证和测试要用到的封装数据
    train, valid = data_util.load_dataset(vocabulary_word2index, vocabulary_word2index_label, FLAGS.sentence_len, FLAGS.valid_portion, FLAGS.data_path)
    test, _= data_util.load_dataset(vocabulary_word2index, vocabulary_word2index_label, FLAGS.sentence_len, 0, FLAGS.test_path)

    # 设定gpu训练
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    with tf.Session(config=config) as sess:
        # 定义卷积核大小
        filter_sizes = list(map(int, FLAGS.filter_sizes.split(",")))
        # 搭建textCNN模型
        textCNN = TextCNN(filter_sizes, FLAGS.num_filters, FLAGS.num_classes, FLAGS.learning_rate, FLAGS.batch_size, FLAGS.decay_steps, FLAGS.decay_rate,
                          FLAGS.sentence_len, vocab_size, FLAGS.embed_size)
        # 初始化tf训练器
        init = tf.global_variables_initializer()
        saver = tf.train.Saver(max_to_keep=1)
        # 确定模型训练模式
        if os.path.exists(FLAGS.ckpt_dir+"checkpoint"):
            print("Restoring Variables from Checkpoint")
            saver.restore(sess, tf.train.latest_checkpoint(FLAGS.ckpt_dir))
        else:
            print('Initializing Variables')
            sess.run(init)
            # 如果用到word2vec词向量，则将向量赋值给textcnn的embedding层
            if FLAGS.use_embedding:
                assign_pretrained_word_embedding(sess, textCNN, word_embeddings)
        # 执行epoch轮次训练
        curr_epoch = sess.run(textCNN.epoch_step)
        for epoch in range(curr_epoch, FLAGS.num_epochs):
            one_epoch_steps = len(train.textlist)/FLAGS.batch_size
            step = 1
            loss, acc = 0.0, 0.0
            while step <= one_epoch_steps:
                # 获取batch数据
                train_x, train_y = train.nextBatch(FLAGS.batch_size)
                # 数据封装
                feed_dict = {textCNN.input_x: train_x, textCNN.input_y: train_y, textCNN.dropout_keep_prob: FLAGS.dropout_keep_prob}#0.5
                # 将此batch数据送入textcnn模型训练，得到loss值
                curr_loss, curr_acc, _ = sess.run([textCNN.loss_val, textCNN.accuracy, textCNN.train_op], feed_dict)
                step += 1
                loss, acc = loss + curr_loss, acc + curr_acc
                if step % 1 == 0:
                    print("Epoch %d\tBatch %d\tTrain Loss:%.3f\tTrain Accuracy:%.3f" % (
                    epoch, step, loss / float(step), acc / float(step)))
            print("going to increment epoch counter....")
            sess.run(textCNN.epoch_increment)
            print(epoch, FLAGS.validate_every, (epoch % FLAGS.validate_every == 0))
            # 程序运行到设定的轮次时，执行验证及测试过程
            if epoch % FLAGS.validate_every == 0:
                eval_loss, eval_acc, eval_f1, eval_precisoin= do_eval(sess, textCNN, valid, FLAGS.batch_size)
                print("Epoch %d Validation Loss:%.3f\tValidation Accuracy: %.3f\tValidation f1 score: %.3f\tValidation precision: %.3f" % (epoch, eval_loss, eval_acc,eval_f1,eval_precisoin))
                test_loss, test_acc, test_f1, test_precisoin = do_eval(sess, textCNN, test, FLAGS.batch_size)
                print("Epoch %d Validation Loss:%.3f\tValidation Accuracy: %.3f\tValidation f1 score: %.3f\tValidation precision: %.3f" % (
                    epoch, test_loss, test_acc, test_f1, test_precisoin))
                # 保存模型
                save_path = FLAGS.ckpt_dir + "model.ckpt"
                saver.save(sess, save_path, global_step=epoch)


# 将数据预处理得到的word embedding词向量赋值给textcnn中的embedding层
def assign_pretrained_word_embedding(sess, textCNN, pre_word_embeding):
    t_assign_embedding = tf.assign(textCNN.Embedding, pre_word_embeding)
    sess.run(t_assign_embedding)

def do_eval(sess, textCNN, eval, batch_size):
    # 获取当前测试数据集的长度
    num_sample = len(eval.textlist)
    eval_loss, eval_acc = 0.0, 0.0
    eval_pred, eval_true = [], []
    input_x = []
    # 定义全部数据步长
    one_epoch_steps = num_sample / batch_size
    # 定义剩余数据数量
    residue_num = num_sample % batch_size
    step = 1

    # 当前step值小于全部数据步长时，将batch size个数据送入textcnn执行测试
    while step <= one_epoch_steps:
        # 获取batch数据
        eval_x, eval_y = eval.nextBatch(FLAGS.batch_size)
        # 数据封装
        feed_dict = {textCNN.input_x: eval_x, textCNN.input_y: eval_y, textCNN.dropout_keep_prob: 1}
        # 将此batch数据送入textcnn模型训练，得到loss值
        curr_eval_loss, curr_eval_acc = sess.run([textCNN.loss_val, textCNN.accuracy], feed_dict)
        # 执行textcnn的预测模式，获得数据的与测试
        pred = sess.run(textCNN.predictions, feed_dict)
        eval_loss, eval_acc = eval_loss + curr_eval_loss, eval_acc + curr_eval_acc
        eval_pred.extend(pred.tolist())
        eval_true.extend(np.array(eval_y))
        input_x.extend(eval_x)
        step += 1
    # 如果还有剩余数据，则将剩余数据全部送入textcnn进行测试
    if residue_num != 0:
        eval_x, eval_y = eval.nextBatch(residue_num)
        feed_dict = {textCNN.input_x: eval_x, textCNN.input_y: eval_y, textCNN.dropout_keep_prob: 1}
        curr_eval_loss, curr_eval_acc = sess.run([textCNN.loss_val, textCNN.accuracy],
                                                 feed_dict)
        pred = sess.run(textCNN.predictions, feed_dict)
        eval_loss, eval_acc = eval_loss + curr_eval_loss, eval_acc + curr_eval_acc
        eval_pred.extend(pred.tolist())
        eval_true.extend(np.array(eval_y))
        input_x.extend(eval_x)
    # 计算预测准确率accuracy
    acc = metrics.accuracy_score(eval_true, eval_pred)
    # 计算f1分数
    f1_score = metrics.f1_score(eval_true, eval_pred,average='macro')
    # 计算预测精确度precision
    precision = metrics.precision_score(eval_true, eval_pred,average='macro')
    return eval_loss / float(step + 1), acc, f1_score,precision


if __name__=='__main__':
    tf.app.run()