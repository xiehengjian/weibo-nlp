import xml.etree.ElementTree as ET
import re
import os
import jieba
import codecs
import sys

## 统一训练集和测试集的标签名，定义标签对应的字典
ch_en_dict = {"D":"none","":"none","愤怒":"anger","悲伤":"sadness",
              "惊讶":"surprise","无":"none","高兴":"happiness",
              "厌恶":"disgust","喜好":"like","恐惧":"fear"}

  # 过滤utl
def filter_url(line):
    pattern = r'http://[a-zA-Z0-9.?/&=:]*'
    results = re.compile(pattern, re.S)
    result = results.sub("", line)
    return result

# 过滤转发人
def filter_people(line):
    items = line.split("//@")
    result = ""
    for name in items:
        name = name.split(":")[-1]
        result += name
    return result

# 过滤特殊标点符号
def filter_punctuation(line):
    pattern = '[’!"#$%&\'()*+,-./:;<=>?@；；：．｜～\≧▽—°' \
              '❄×▲♥♀☀●巜「」☕／↓→<=>?@⁄•ω★☕·、…★、' \
              '…【】《》『』（）？“”‘’！[\\]^_`{|}~]+'
    result = re.sub(pattern,'',line)
    return result

## 提取微博文本，输出all、trian、test文件
def Analysis_xml(file_dir, out_dir):
    # 遍历train_data.xml和test_data.xml文件
    for fname in os.listdir(file_dir):
        # 确定是train还是test文件
        fname_type = fname.strip().split('_')[0]
        # 保存文件路径，train或者test
        output_path = os.path.join(out_dir, fname_type)
        # 保存all文件的路径
        output_path_all = os.path.join(out_dir,"all")
        f_out = open(output_path,'a')
        f_out_all = open(output_path_all,'a')


        # 读取并解析xml文件
        file_path = os.path.join(file_dir, fname)
        tree = ET.parse(file_path)
        root = tree.getroot()
        weibos = root.findall("weibo")
        # 遍历每条weibo数据
        for weibo in weibos:
            outstr = ''
            # 获取情绪标签
            label = weibo.get('emotion-type')
            if label == None:
                label = weibo.get("emotion-type1")
            # 调用定义好的标签字典，统一标签名称
            if label in ch_en_dict:
                label = ch_en_dict[label]


            # 获取当前weibo数据中的微博文本
            sentences = weibo.findall("sentence")
            weibo_text = ''
            for sentence in sentences:
                try:
                    # 对微博文本过滤掉utl、转发人、特殊标点符号等信息
                    text = sentence.text
                    text = filter_url(text)
                    text = filter_people(text)
                    text = filter_punctuation(text)
                    weibo_text += text +" "
                    # 对微博文本进行jieba分词处理
                    words = jieba.cut(weibo_text)
                    # 分词与分词直接用空格相隔开
                    outstr = ' '.join(words)
                except Exception:
                    pass

            # 将所有经分词处理后的句子写入到all文件中
            f_out_all.write(outstr+'\n')
            if label=="":
                print(weibo.get('id'))
            # 将经分词处理后的句子和对应的情绪标签用“|”符号相拼接
            outstr = outstr+'|'+label
            # 将所有经分词处理后的句子及对应的情绪标签写入到train或test文件中
            f_out.write(outstr+'\n')


## 获取全部分词结果并保存至vocab文件，输出标签文件label
def get_label_vocab(file_dir):
    # 创建两个空集合，用与保存vocab和label数据内容
    vocabset = set()
    labelset = set()
    # 遍历上一步生成的train、test文件
    for fname in os.listdir(file_dir):
        if fname != "all":
            path = os.path.join(file_dir,fname)

            # 读取文件中的每一行数据
            for line in codecs.open(path):
                # 获取分词处理后的句子
                _split = line.strip().split('|')
                # 获取当前数据的情感标签，并添加到labelset集合中
                labelset.add(_split[1])
                # 将分词处理后的句子，按照词的形式保存到vocabset集合中
                words = _split[0].split(' ')
                [vocabset.add(w.strip()) for w in words]
    # 将vocabset集合保存到vocab文件
    f_vocab = codecs.open(os.path.join(file_dir,'vocab'),'a')
    [f_vocab.write(vocab+'\n') for vocab in vocabset if vocab!=""]
    # 将labelset集合保存到label文件
    f_label = codecs.open(os.path.join(file_dir,'label'),'a')
    [f_label.write(label+'\n') for label in labelset if label!=""]


if __name__=="__main__":
    inpdir = 'data/'
    outdir = 'pre'
    Analysis_xml(inpdir,outdir)
    get_label_vocab(outdir)
