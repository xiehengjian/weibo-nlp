# -*- coding:utf-8 -*-
from collections import Counter, defaultdict
import codecs
import os
import random


# 构建分词组合的滑动窗口
def window(sentence, start_index, end_index):
    last_index = len(sentence) + 1
    selected_tokens = sentence[max(start_index, 0):min(end_index, last_index) + 1]
    return selected_tokens

# 返回所有分词组合的情况
def context_windows(sentence, left_size, right_size):
    for i, word in enumerate(sentence):
        start_index = i - left_size
        end_index = i + right_size
        left_context = window(sentence, start_index, i - 1)
        right_context = window(sentence, i + 1, end_index)
        yield (left_context, word, right_context)

## 构建分词列表的共现矩阵
def get_cooccurrence_matrix(corpus, min_occurrences, vocab_size, left_size, right_size):
    word_counts = Counter()
    cooccurrence_counts = defaultdict(float)
    # 遍历所有句子
    for sentence in corpus:
        # 统计词频
        word_counts.update(sentence)
        # 遍历所有分词组合的情况，并获取当前词及其对应的前置词、后置词
        for l_context, word, r_context in context_windows(sentence, left_size, right_size):
            # 计算glove中的衰减系数，即共现矩阵中的元素值
            for i, context_word in enumerate(l_context[::-1]):
                cooccurrence_counts[(word, context_word)] += 1 / (left_size-i)
            for i, context_word in enumerate(r_context):
                cooccurrence_counts[(word, context_word)] += 1 / (right_size-i)

    # 筛选出词频大于min_occurrences的分词
    words = [word for word, count in word_counts.most_common(vocab_size)
                    if count >= min_occurrences]
    # 将分词转为id编号
    word2id = {word: i for i, word in enumerate(words)}
    id2word = {i:word for i, word in enumerate(words)}
    # 将共现矩阵中的分词转为对应的id编号
    cooccurrence_matrix = {
        (word2id[words[0]], word2id[words[1]]): count
        for words, count in cooccurrence_counts.items()
        if words[0] in word2id and words[1] in word2id}

    return cooccurrence_matrix, word2id, id2word

# 构建一个共现矩阵类
class CooccurenceMatrix:
    def __init__(self,i_index, j_index, count):
        self.i_index = i_index
        self.j_index = j_index
        self.count = count

## 封装数据集，构建训练数据迭代器
class Dataset:
    def __init__(self, CMlist, currentIdx):
        self.CMlist = CMlist
        self.currentIdx = currentIdx

    # 编写迭代器迭代方法，返回截取的一个批次的数据
    def next_batch(self, batch_size):
        # 如果共现矩阵列表的长度小于batch_size的设定，那么直接打乱数据
        if self.currentIdx + batch_size > len(self.CMlist):
            self.currentIdx = 0
            random.shuffle(self.CMlist)
        startId = self.currentIdx
        i_indexs = []
        j_indexs = []
        counts = []
        # 按照batch_size的大小分别添加前置词编号列表、分词编号到i_indexs和j_indexs列表中
        for _ in range(batch_size):
            print(len(self.CMlist))
            i_indexs.append(self.CMlist[startId+_].i_index)
            j_indexs.append(self.CMlist[startId+_].j_index)
            counts.append(self.CMlist[startId+_].count)
        self.currentIdx += batch_size
        return i_indexs, j_indexs, counts

## 加载数据集的接口
def load_dataset(file_dir,min_occurrences, vocab_size, left_size, right_size):
    corpus = []
    # 读取all文件
    for fname in os.listdir(file_dir):
        for line in codecs.open(os.path.join(file_dir, fname), 'r', 'utf8'):
            # 获取all文件中的所有分词处理后的句子，并汇总成一个包含所有分词句子的列表corpus
            words = line.strip().split()
            corpus.append(words)
    # 为每个分词附上id编号，构建分词到编号、编号到分词的查询字典，并构建分词列表的共现矩阵
    cooccurrence_matrix, word2id, id2word = get_cooccurrence_matrix(corpus,min_occurrences, vocab_size, left_size, right_size)
    # 将共线矩阵按照元组形式重新整理
    cooccurrences = [(word_ids[0], word_ids[1], count)
                     for word_ids, count in cooccurrence_matrix.items()]
    CMlist = []
    for (i_index, j_index, counts) in cooccurrences:
        CM = CooccurenceMatrix(i_index, j_index, counts)
        CMlist.append(CM)
    # 打乱数据顺序
    random.shuffle(CMlist)
    # 封装数据集，构建训练数据迭代器
    train = Dataset(CMlist,0)
    return train, word2id, id2word

