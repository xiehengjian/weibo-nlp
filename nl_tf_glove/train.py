# -*- coding:utf-8 -*-
import tensorflow as tf
import data_util
from model import Glove

FLAGS = tf.app.flags.FLAGS
tf.app.flags.DEFINE_integer("embedding_size",100, "size of word embedding")
tf.app.flags.DEFINE_float("alpha",0.75,"the value of parameter alpha")
tf.app.flags.DEFINE_integer("x_max",100, "the value of parameter x_max")
tf.app.flags.DEFINE_integer("vocab_size",50000,"")
tf.app.flags.DEFINE_integer("left_size",2,"")
tf.app.flags.DEFINE_integer("right_size",2,"")
tf.app.flags.DEFINE_integer("batch_size", 3,"batch size")
tf.app.flags.DEFINE_string("file_dir","data/","the path of the train data")
tf.app.flags.DEFINE_integer("min_occurrences", 3, "The minimum frequency of words")

tf.app.flags.DEFINE_float("learning_rate",0.01,"learning rate")
tf.app.flags.DEFINE_integer("decay_steps",6000,"how many steps before decay learning rate.")
tf.app.flags.DEFINE_float("decay_rate", 0.65, "Rate of decay for learning rate.")


tf.app.flags.DEFINE_integer("num_epochs", 3, "number of epochs to run.")
tf.app.flags.DEFINE_string("ckpt_dir","checkpoint/","checkpoint location for the model")

def main(_):
    # 获取数据预处理结果
    train, word2id, id2word = data_util.load_dataset(FLAGS.file_dir, FLAGS.min_occurrences,FLAGS.vocab_size, FLAGS.left_size, FLAGS.right_size)
    # 获取词汇量大小
    vocab_size = len(word2id)
    print("vocab_size:",vocab_size)
    # 设定gpu训练
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True

    with tf.Session(config = config) as sess:
        # 搭建glove模型
        gv = Glove(FLAGS.embedding_size,vocab_size,FLAGS.alpha, FLAGS.x_max,
                   FLAGS.batch_size, FLAGS.learning_rate, FLAGS.decay_steps, FLAGS.decay_rate)
        # 初始化tf训练器
        init = tf.global_variables_initializer()
        saver = tf.train.Saver(max_to_keep=1)
        print('Initializing Variables')
        sess.run(init)
        # 执行epoch轮次训练
        curr_epoch = sess.run(gv.epoch_step)
        for epoch in range(curr_epoch, FLAGS.num_epochs):
            one_epoch_steps = len(train.CMlist) / FLAGS.batch_size
            step = 0
            while step <= one_epoch_steps:
                # 获取batch数据
                i_index, j_index, count = train.next_batch(FLAGS.batch_size)
                # 数据封装
                feed_dict = {gv.current_input:i_index, gv.context_input:j_index,gv.cooccurrence_count:count}
                # 将此batch数据送入glove模型训练，得到loss值
                curr_loss, _ = sess.run([gv.loss_val, gv.train_op],feed_dict)
                step += 1
                print(curr_loss)
            # 保存模型
            save_path = FLAGS.ckpt_dir + "model.ckpt"
            saver.save(sess, save_path, global_step=epoch)

if __name__=="__main__":
    tf.app.run()
