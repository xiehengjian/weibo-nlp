# -*- coding:utf-8 -*-
import tensorflow as tf

class Glove:

    def __init__(self, embedding_size, vocab_size,
                 alpha =3/4, x_max=100, batch_size=512, learning_rate=0.1,decay_steps=60000, decay_rate=0.67):

        self.embedding_size = embedding_size  # 词向量维度大小
        self.batch_size = batch_size          # batch大小
        self.vocab_size = vocab_size          # 容纳最多词汇量
        self.alpha = alpha                    # glove模型中权重函数的幂值
        self.x_max = x_max                    # glove模型中权重函数的自变量最大值
        self.global_step = tf.Variable(0, trainable=False, name="Global_Step")
        self.epoch_step = tf.Variable(0, trainable=False, name="Epoch_Step")
        self.epoch_increment = tf.assign(self.epoch_step, tf.add(self.epoch_step, tf.constant(1)))
        self.decay_steps = decay_steps        # 学习率调整步长
        self.decay_rate = decay_rate          # 学习率调整率
        self.learning_rate = learning_rate    # 学习率
        self.current_input = tf.placeholder(tf.int32, shape=[self.batch_size],
                                            name="current_words")
        self.context_input = tf.placeholder(tf.int32, shape=[self.batch_size],
                                            name="context_words")
        self.cooccurrence_count = tf.placeholder(tf.float32, shape=[self.batch_size],
                                                 name="cooccurrence_count")

        self.instantiate_weights()
        self.loss_val = self.loss()
        self.train_op = self.train()


    def instantiate_weights(self):
        self.x_max = tf.constant([self.x_max], dtype=tf.float32,
                                name='x_max')
        self.alpha = tf.constant([self.alpha], dtype=tf.float32,
                                     name="alpha")
        # 初始化中心词的输入，定义为vocab_size*embedding_size的随机矩阵，取值范围为-1到1
        self.current_embeddings = tf.Variable(
            tf.random_uniform([self.vocab_size, self.embedding_size], 1.0, -1.0),
            name="current_embeddings")
        # 初始化上下文词的输入，定义为vocab_size*embedding_size的随机矩阵，取值范围为-1到1
        self.context_embeddings = tf.Variable(
            tf.random_uniform([self.vocab_size, self.embedding_size], 1.0, -1.0),
            name="context_embeddings")
        # 初始化偏置bias，定义为vocab_size长的随机矩阵，取值范围为-1到1
        self.current_biases = tf.Variable(tf.random_uniform([self.vocab_size], 1.0, -1.0),
                                   name='current_biases')

    # 模型公式及loss函数设定
    def loss(self):
        # 输入中心词、上下文词
        current_embedding = tf.nn.embedding_lookup([self.current_embeddings], self.current_input)
        context_embedding = tf.nn.embedding_lookup([self.context_embeddings], self.context_input)
        # 中心词和上下文词的两个偏置bias
        current_bias = tf.nn.embedding_lookup([self.current_biases], self.current_input)
        context_bias = tf.nn.embedding_lookup([self.current_biases], self.context_input)

        # 定义权重函数
        object_function_factor1 = tf.minimum(
            tf.pow(
                tf.div(self.cooccurrence_count, self.x_max),
                self.alpha),1.0)
        # 构建词向量和共现矩阵之间的近似关系
        object_function_factor2 = tf.square(
            tf.reduce_sum(tf.multiply(current_embedding, context_embedding), 1) + current_bias + context_bias - \
            tf.log(tf.to_float(self.cooccurrence_count)))
        # glove的loss公式
        loss = tf.reduce_sum(tf.multiply(object_function_factor1, object_function_factor2))

        return loss

    # 执行训练的配置
    def train(self):
        # 学习率的封装配置
        learning_rate = tf.train.exponential_decay(self.learning_rate, self.global_step, self.decay_steps,
                                                   self.decay_rate, staircase=True)
        # 训练优化算法配置
        train_op = tf.contrib.layers.optimize_loss(self.loss_val, global_step=self.global_step,
                                                   learning_rate=learning_rate, optimizer="Adagrad")
        return train_op
