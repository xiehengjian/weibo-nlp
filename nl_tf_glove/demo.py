mport glove
import codecs

# 读取数据
class Sentences(object):
    def __init__(self, file_path):
        self.file_path = file_path
    def __iter__(self):
        # 读取all中的每行数据
        for line in codecs.open(self.file_path, 'r','utf8'):
            # 迭代返回每行数据中的每个分词
            yield line.split()

if __name__=="__main__":

    # 定义输入输出
    inpath = 'data/all'
    outpath1 = 'glove.bin'

    # 读取all文件中的数据
    sentense = Sentences(inpath)

    # 将数据转换为glove模型所需的共现矩阵格式
    corpus_model = glove.Corpus()
    corpus_model.fit(sentense, window=5)
    print('Dict size: %s' % len(corpus_model.dictionary))
    print('Collocations: %s' % corpus_model.matrix.nnz)

    # 调用glove模型进行训练
    ge = glove.Glove(no_components=100, learning_rate=0.01)
    ge.fit(corpus_model.matrix, epochs=20,
              no_threads=1, verbose=True)
    # 保存模型
    ge.save(outpath1)

    #验证一下“喜欢”的相似词
    ge.add_dictionary(corpus_model.dictionary)
    # number=3表示输出3-1=2个相似词
    ms = ge.most_similar('喜欢', number=3)
    print(ms)
