import tensorflow as tf
import math


class NNLM:
    # 定义NNLM模型相关训练参数
    def __init__(self, embedding_size=100,window_size=5,batch_size=128, vocab_size=50000, hidden_size=256,grad_clip=10,
                 learning_rate=0.01,decay_steps=60000,decay_rate=0.6):
        self.embedding_size = embedding_size
        self.window_size = window_size     #分词窗口大小
        self.batch_size = batch_size       # batch大小
        self.vocab_size = vocab_size       # 容纳最多词汇量
        self.hidden_size = hidden_size     #模型的隐藏层神经元数
        self.grad_clip = grad_clip         # 梯度裁剪值
        self.learning_rate = learning_rate  # 学习率
        self.decay_steps = decay_steps      # 学习率调整步长
        self.decay_rate = decay_rate        # 学习率调整率

        self.global_step = tf.Variable(0, trainable=False, name="Global_Step")
        self.epoch_step = tf.Variable(0, trainable=False, name="Epoch_Step")

        self.input_data = tf.placeholder(tf.int32, [batch_size, window_size])
        self.targets = tf.placeholder(tf.int32, [batch_size])

        self.instantiate_weights()
        self.logits = self.inference()
        self.loss_val = self.loss()
        self.train_op = self.train()

    # NNLM的实例层
    def instantiate_weights(self):
        embeddings = tf.Variable(tf.random_uniform([self.vocab_size, self.embedding_size], -1.0, 1.0))
        #输入层
        self.embeddings = tf.nn.l2_normalize(embeddings, 1)
        #权重设定
        self.weight_h = tf.Variable(tf.truncated_normal([self.window_size * self.embedding_size+ 1, self.hidden_size],
                                                   stddev=1.0 / math.sqrt(self.hidden_size)))
        # softmax层设定
        self.softmax_w = tf.Variable(tf.truncated_normal([self.window_size * self.embedding_size, self.vocab_size],
                                                    stddev=1.0 / math.sqrt(self.window_size * self.embedding_size)))
        self.softmax_u = tf.Variable(tf.truncated_normal([self.hidden_size + 1, self.vocab_size],
                                                    stddev=1.0 / math.sqrt(self.hidden_size)))
    # NNLM模型的前向函数（网络结构）
    def inference(self):
        # 输入
        inputs_emb = tf.nn.embedding_lookup(self.embeddings, self.input_data)
        inputs_emb = tf.reshape(inputs_emb, [-1, self.window_size * self.embedding_size])
        inputs_emb_add = tf.concat( [inputs_emb, tf.ones(tf.stack([tf.shape(self.input_data)[0], 1]))],1)
        # tahn层
        inputs = tf.tanh(tf.matmul(inputs_emb_add, self.weight_h))
        inputs_add = tf.concat([inputs, tf.ones(tf.stack([tf.shape(self.input_data)[0], 1]))],1)
        #经softmax输出
        outputs = tf.matmul(inputs_add, self.softmax_u) + tf.matmul(inputs_emb, self.softmax_w)
        outputs = tf.clip_by_value(outputs, 0.0, self.grad_clip)
        outputs = tf.nn.softmax(outputs)
        return outputs

    # loss函数设定
    def loss(self, l2_lambda=0.0001):
        # 设定交叉熵损失函数
        losses = tf.nn.sparse_softmax_cross_entropy_with_logits(labels=self.targets,
                                                                logits=self.logits)
        loss = tf.reduce_mean(losses)
        # 添加l2正则项
        l2_losses = tf.add_n(
            [tf.nn.l2_loss(v) for v in tf.trainable_variables() if 'bias' not in v.name]) * l2_lambda
        # 总loss函数为交叉熵损失和l2正则项个和
        loss = loss + l2_losses
        return loss

    # 执行训练的配置
    def train(self):
        # 学习率的封装配置
        learning_rate = tf.train.exponential_decay(self.learning_rate, self.global_step, self.decay_steps,self.decay_rate, staircase=True)
        # 训练优化算法配置
        train_op = tf.contrib.layers.optimize_loss(self.loss_val, global_step=self.global_step,learning_rate=learning_rate, optimizer="Adagrad")
        return train_op
