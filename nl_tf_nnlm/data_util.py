
from collections import Counter
import codecs
import os
import random

## 按照词频大小降序排列分词，并为每个分词附上id编号
def create_vocabdict(corpus,vocab_size,min_occurrences):
    # 统计词频
    word_counts = Counter()
    for sentence in corpus:
        word_counts.update(sentence)
    # 筛选出词频最高的vocab_size个单词，且词频数值大于min_occurrences
    words = [word for word, count in word_counts.most_common(vocab_size)
             if count >= min_occurrences]

    word2id = {word: i+1 for i, word in enumerate(words)}
    id2word = {i+1: word for i, word in enumerate(words)}
    word2id['PAD_ID'] = 0
    id2word[0]='PAD_ID'
    return word2id, id2word


## 构建分词列表的共现矩阵

# 构建分词组合的滑动窗口
def window(sentence, start_index, end_index):
    last_index = len(sentence) + 1
    selected_tokens = sentence[max(start_index, 0):min(end_index, last_index) + 1]
    return selected_tokens

# 返回所有分词组合的情况
def context_windows(sentence, left_size):
    for i, word in enumerate(sentence):
        start_index = i - left_size
        left_context = window(sentence, start_index, i - 1)
        yield (left_context, word)

def get_cooccurrence_matrix(corpus, word2id,window_size):
    cooccurenceMatrixlist=[]
    # 遍历每个句子
    for sentence in corpus:
        # 遍历所有分词组合的情况，并获取当前词及其对应的前置词
        for l_context, word in context_windows(sentence, window_size):
            if word in word2id:
                # 获取当前词的编号
                target = word2id[word]
                contexlist=[0]*window_size
                for i, context_word in enumerate(l_context):
                    if context_word in word2id:
                        # 将前置词所对应的编号存放到contextlist列表中
                        contexlist[i] = word2id[context_word]
                # 将当前词的编号、前置词对应的编号列表封装到共现矩阵类中
                cm = CooccurenceMatrix(contexlist,target)
            # 返回所有分词组合的共现矩阵列表
            cooccurenceMatrixlist.append(cm)
    return cooccurenceMatrixlist

# 构建一个共现矩阵类
class CooccurenceMatrix:
    def __init__(self,context_list, target):
        self.context_list = context_list
        self.target = target


## 封装数据集，构建训练数据迭代器
class Dataset:
    def __init__(self, CMlist, currentIdx):
        self.CMlist = CMlist
        self.currentIdx = currentIdx
    # 编写迭代器迭代方法，返回截取的一个批次的数据
    def next_batch(self, batch_size):
        # 如果共现矩阵列表的长度小于batch_size的设定，那么直接打乱数据
        if self.currentIdx + batch_size > len(self.CMlist):
            self.currentIdx = 0
            random.shuffle(self.CMlist)
        startId = self.currentIdx
        i_indexs = []
        j_indexs = []
        # 按照batch_size的大小分别添加前置词编号列表、分词编号到i_indexs和j_indexs列表中
        for _ in range(batch_size):
            i_indexs.append(self.CMlist[startId+_].context_list)
            j_indexs.append(self.CMlist[startId+_].target)
        self.currentIdx += batch_size
        return i_indexs, j_indexs


## 加载数据集的接口
def load_dataset(file_dir,min_occurrences, vocab_size, window_size):
    corpus = []
    # 获取all文件
    for fname in os.listdir(file_dir):
        # 读取all文件
        for line in codecs.open(os.path.join(file_dir, fname), 'r','utf8'):
            #获取all文件中的所有分词处理后的句子，并汇总成一个包含所有分词句子的列表corpus
            words = line.strip().split()
            corpus.append(words)
    # 每个分词附上id编号，构建分词到编号、编号到分词的查询字典
    word2id, id2word = create_vocabdict(corpus,vocab_size,min_occurrences)
    # 构建分词列表的共现矩阵
    CMlist = get_cooccurrence_matrix(corpus, word2id,window_size)
    # 打乱数据顺序
    random.shuffle(CMlist)
    # 封装数据集，构建训练数据迭代器
    train = Dataset(CMlist,0)
    return train, word2id, id2word

